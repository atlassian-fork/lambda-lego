function usage () {
    echo "Usage: $0 create|update [--role <role-arn>] [-h]"
    echo "Builds an executable, zips and deploys it. Specify create if building for first time; must also specify role on first time"
    echo "Subsequent executions specify update, no role required"
    echo "     -h   show this help message"
    echo "     --role the ARN of the execution role to attach the lambda when creating it"
}

LAMBDA_ENV="Variables={ACME_USER_SM_KID=$ACME_USER_SM_KID,ADMIN_EMAIL=$ADMIN_EMAIL,CERT_BUCKET=$CERT_BUCKET,CA_ENDPOINT=$CA_ENDPOINT,ACME_CHALLENGE_S3_KEY=$ACME_CHALLENGE_S3_KEY,CERT_DOMAIN=$CERT_DOMAIN,DRY_RUN=$DRY_RUN,AWS_LAMBDA_REGION=$AWS_LAMBDA_REGION,CLUSTER_HTTPS_PORT=$CLUSTER_HTTPS_PORT,ALB_ARN=$ALB_ARN,CERT_S3_KEY=$CERT_S3_KEY}"

if [[ $# -eq 0 ]] ;  then
    usage
    exit 1
fi

if [[ $AWS_REGION == "" ]] ; then
    AWS_REGION="us-west-2"
fi

for (( i=1; i<=$#; i++)); do
    arg="${!i}"
    if [[ $arg == "-h" ]] ; then
        usage
        exit 0
    fi
    if [[ $arg == "--role" ]] ; then
       j=$((i+1))
       role="${!j}"
    fi
done

if env | grep AWS_SECRET_ACCESS_KEY ; then
    echo "Building lambda-lego..."
    GOOS=linux GOARCH=amd64 go build -o lambda-lego cmd/lambda-lego.go
    if [[ $? -ne 0 ]] ; then
        echo "Could not build lambda-lego"
        exit 1
    fi

    GOOS=linux GOARCH=amd64 go build -o lambda-lego-listener cmd/lambda-lego-listener.go
    if [[ $? -ne 0 ]] ; then
        echo "Could not build lambda-lego-listener"
        exit 1
    fi

    echo "Zipping lambda-lego..."
    zip lambda-lego.zip lambda-lego

    echo "Zipping lambda-lego listener..."
    zip lambda-lego-listener.zip lambda-lego-listener

    if [[ $1 == "create" ]] ; then
        echo "Creating function"

        if [[ $role == "" ]] ; then
            echo "Must specify role when creating function. Call again with --role <role ARN>"
            exit 1
        fi

        echo "Uploading functions to AWS with role: $role"
        echo "Uploading lambda-lego..."
        aws lambda create-function \
          --region $AWS_REGION \
          --function-name lambda-lego \
          --memory 128 \
          --role $role \
          --environment $LAMBDA_ENV \
          --runtime go1.x \
          --zip-file fileb://lambda-lego.zip \
          --handler lambda-lego

        echo "Uploading lambda-lego listener"
        aws lambda create-function \
          --region $AWS_REGION \
          --function-name lambda-lego-listener \
          --memory 128 \
          --role $role \
          --environment $LAMBDA_ENV \
          --runtime go1.x \
          --zip-file fileb://lambda-lego-listener.zip \
          --handler lambda-lego-listener

    elif [[ $1 == "update" ]] ; then
        echo "Updating functions"
        aws lambda update-function-code \
          --region $AWS_REGION \
          --function-name lambda-lego \
          --zip-file fileb://lambda-lego.zip

        aws lambda update-function-code \
          --region $AWS_REGION \
          --function-name lambda-lego-listener \
          --zip-file fileb://lambda-lego-listener.zip

    else
        echo "You must specify create or update"
        usage
    fi

    rm lambda-lego lambda-lego.zip lambda-lego-listener lambda-lego-listener.zip

else
    echo "Could not detect AWS_SECRET_ACCESS_KEY in env. Are you authenticated?"
    exit 1
fi

function usage () {
    echo "Usage: $0 create|update [--role <role-arn>] [-h]"
    echo "Builds an executable, zips and deploys it. Specify create if building for first time; must also specify role on first time"
    echo "Subsequent executions specify update, no role required"
}
